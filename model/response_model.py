import json
import decimal

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return str(o)
        return super(DecimalEncoder, self).default(o)

class Response:

    def json_response(code, body = None, origin = '*'):
        response = {
            "statusCode": code,
            "body": json.dumps(body, cls=DecimalEncoder),
            "headers": {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': origin
            }
        }
        return response