import json
from model.loyalty_card_model import HelperLoyaltyCard, IncompleteDataException, LoyaltyCard
from model.response_model import Response
import boto3
import botocore

class LoyaltyCardController:

    def create_loyalty_card(event, context, LOYALTY_DDB):
        dynamo_db = boto3.resource('dynamodb')
        dynamo_db_table = dynamo_db.Table(LOYALTY_DDB)
        body = json.loads(event['body'])

        try:
            HelperLoyaltyCard.perform(body)
            ddb_response = dynamo_db_table.put_item(
                Item=body
            )

            response = Response.json_response(200, body)
            return response
        except IncompleteDataException as e:
            print(e)
            response = Response.json_response(400, {"error": "Bad Request"})
            return response

    def get_loyalty_card_by_id(event, context, LOYALTY_DDB):
        dynamo_db = boto3.resource('dynamodb')
        dynamo_db_table = dynamo_db.Table(LOYALTY_DDB)
        
        if "pathParameters" not in event:
            response = Response.json_response(400, {"error": "Bad Request"})
            return response

        if "id" not in event.get("pathParameters"):
            response = Response.json_response(400, {"error": "Bad Request"})
            return response

        loyalty_card_id = event['pathParameters']['id']
        try:
            ddb_response = dynamo_db_table.get_item(
                Key={
                    'id': int(loyalty_card_id)
                }
            )
            response = Response.json_response(200, ddb_response.get('Item'))
            return response

        except dynamo_db.meta.client.exceptions.ResourceNotFoundException as e:
            print(e)
            response = Response.json_response(400, {"error": "Bad Request"})
            return response

    def get_all_loyalty_card(event, context, LOYALTY_DDB):
        dynamo_db = boto3.resource('dynamodb')
        dynamo_db_table = dynamo_db.Table(LOYALTY_DDB)

        try:
            ddb_response = dynamo_db_table.scan()
            data = ddb_response.get('Items')

            while 'LastEvaluatedKey' in ddb_response:
                ddb_response = dynamo_db_table.scan(
                    ExclusiveStartKey=ddb_response['LastEvaluatedKey']
                )
                data.extend(ddb_response.get('Items'))

            response = Response.json_response(200, data)
            return response

        except dynamo_db.meta.client.exceptions.ResourceNotFoundException as e:
            print(e)
            response = Response.json_response(400, {"error": "Bad Request"})
            return response
