import json
import os
from controller.loyalty_card_controller import LoyaltyCardController

LOYALTY_DDB = os.environ['LOYALTY_DDB']

def create_loyalty_card(event, context):

    response = LoyaltyCardController.create_loyalty_card(event, context, LOYALTY_DDB)
    return response

def get_loyalty_card_by_id(event, context):

    response = LoyaltyCardController.get_loyalty_card_by_id(event, context, LOYALTY_DDB)
    return response

def get_all_loyalty_card(event, context):

    response = LoyaltyCardController.get_all_loyalty_card(event, context, LOYALTY_DDB)
    return response
